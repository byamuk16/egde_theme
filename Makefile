render:
	tutor config render --extra-config ./config.yml ./theme "$$(tutor config printroot)/env/build/openedx/themes/egde_theme"

rebuild:
	git pull
	tutor local stop
	tutor config render --extra-config ./config.yml ./theme "$$(tutor config printroot)/env/build/openedx/themes/egde_theme"
	tutor images build -a EDX_PLATFORM_VERSION="open-release/koa.3" openedx && tutor local start -d

localbuild:
	tutor local stop
	tutor config render --extra-config ./config.yml ./theme "$$(tutor config printroot)/env/build/openedx/themes/egde_theme"
	tutor images build -a EDX_PLATFORM_VERSION="open-release/koa.3" openedx && tutor local start -d

watch: render
	while true; do inotifywait -e modify --recursive config.yml ./theme; $(MAKE) render; done
