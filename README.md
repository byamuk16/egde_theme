# Egdelms theme

Egdelms theme is a template that was created to customize the Open edx for Egde to meet the requirement specified in the preliminary project (see documentation). For fother documetaion please refer to https://docs.tutor.overhang.io/ and https://github.com/edx/edx-platform


![User dashboard](documentation/user_dashboard.png)

**Note**: This version of the Egdelm theme is compatible with the Koa release of Open edX. It was specially developed to be used with Tutor https://docs.tutor.overhang.io (at least v11.2.6).

You can view the current version at http://egdelms.com

## Local development

You will need to have tutor, docker and docker-compose installed on your system. For windows users we recomend using Windows [Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/) (WSL).

- [Install docker](https://docs.docker.com/get-docker/) 
- [Install docker-compose](https://docs.docker.com/compose/install/)

```sh
sudo curl -L "https://github.com/overhangio/tutor/releases/download/v11.2.8/tutor-$(uname -s)_$(uname -m)" -o /usr/local/bin/tutor

sudo chmod 0755 /usr/local/bin/tutor

tutor local quickstart
```


Clone the theme repository

    git clone git@bitbucket.org:Egder/egdeskolen.git

Render your theme, rebuild the openedx docker images and restart the build platform.
```sh
make localbuild
```


## Customization


By default, the ``/about`` and ``/contact`` pages contain a simple line of text. Feel free to add your own content". The open edx is built on Django, a high-level python web framework, it users the MTV-architecture pattern.
You may want to customize existing django or edx pages, see https://github.com/edx/edx-platform/ for å referese

In the description below, we show how to override just any of the static templates used in Open edX.

The static templates used by Open edX to render those pages are all stored in the [`edx-platform/lms/templates/static_templates`](https://github.com/edx/edx-platform/) folder. To override those templates, you should add your own in the following folder:

    ls "$(tutor config printroot)/env/build/openedx/themes/egdeskolen/lms/templates/static_templates"

For instance, edit the "donate.html" file in this directory. We can derive the content of this file from the contents of the `donate.html` static template in edx-platform:

```html

<%page expression_filter="h"/>
<%! from django.utils.translation import ugettext as _ %>
<%inherit file="../main.html" />

<%block name="pagetitle">${_("Donate")}</%block>

<main id="main" aria-label="Content" tabindex="-1">
    <section class="container about">
        <h1>
            <%block name="pageheader">${page_header or _("Donate")}</%block>
        </h1>
        <p>
            <%block name="pagecontent">Add a compelling message here, asking for donations.</%block>
        </p>
    </section>
</main>
```
This new template will then be used to render the ``/donate`` url.

## Publish to a production server

We have create a bitbucket pipeline, that can be used to deploy to production everytime you push to this repository.
From your development machinge push to repo (asuming your have access), then check piplene and confirm to deploy the application. 
```sh
# Add changes
git add .

# Commit changes
git commit -m "<some changes>"

# Push changes to origin
git push
```
The ``make rebuild`` command will be executed to change and build the edx containers on the server, see bitbucket-pipelines.yml for a more detailed view.
This command will be executed everytime you confirm a commit in bitbucket pipeline. We recommend setting up a pipeline for the stage version.



## License

This work is licensed under the terms of the GNU Affero General Public License (AGPL)

https://bitbucket.org/Egder/egdeskolen/src/master/LICENSE.txt